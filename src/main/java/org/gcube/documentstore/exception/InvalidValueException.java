package org.gcube.documentstore.exception;

public class InvalidValueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2449589259703195853L;
	
	public InvalidValueException() {
		super();
	}
}
