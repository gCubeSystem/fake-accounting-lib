/**
 * 
 */
package org.gcube.documentstore.records.implementation;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import org.gcube.documentstore.exception.InvalidValueException;
import org.gcube.documentstore.records.Record;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class AbstractRecord implements Record {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8746066891880733502L;

	@Override
	public SortedSet<String> getQuerableKeys() throws Exception {
		return new TreeSet<>();
	}
	
	public AbstractRecord() {
	}
	
	public AbstractRecord(Map<String,? extends Serializable> properties) throws InvalidValueException {
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getRequiredFields() {
		return new HashSet<String>();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getComputedFields() {
		return new HashSet<String>();
	}
	
	public Set<String> getAggregatedFields() {
		return new HashSet<String>();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getId() {
		return UUID.randomUUID().toString();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setId(String id) throws InvalidValueException {

	}
	
	public static Calendar timestampToCalendar(long millis) {
		return Calendar.getInstance();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Calendar getCreationTime() {
		return Calendar.getInstance();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCreationTime(Calendar creationTime) throws InvalidValueException {
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String,Serializable> getResourceProperties() {
		return new HashMap<String,Serializable>();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setResourceProperties(Map<String,? extends Serializable> properties) throws InvalidValueException {

	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Serializable getResourceProperty(String key) {
		return "TEST";
	}
	
	@Override
	public void removeResourceProperty(String key) {
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setResourceProperty(String key, Serializable value) throws InvalidValueException {

	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validate() throws InvalidValueException {
		
	}
	
	@Override
	public String toString() {
		return "{}";
	}
	
	
	@Override
	public int compareTo(Record record) {
		return 0;
	}
	
}
