/**
 * 
 */
package org.gcube.accounting.datamodel.basetypes;

import java.io.Serializable;
import java.net.URI;
import java.util.Map;

import org.gcube.accounting.datamodel.BasicUsageRecord;
import org.gcube.documentstore.exception.InvalidValueException;

public abstract class AbstractStorageStatusRecord extends BasicUsageRecord {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8414496937550686240L;

	public enum DataType {
		STORAGE, TREE, GEO, DATABASE, OTHER
	}
	
	public AbstractStorageStatusRecord() {
	}
	
	public AbstractStorageStatusRecord(Map<String,? extends Serializable> properties) throws InvalidValueException {
	}
	
	private static final String ABSTRACT_TO_REPLACE = "Abstract";
	
	@Override
	public String getRecordType() {
		return AbstractStorageStatusRecord.class.getSimpleName().replace(ABSTRACT_TO_REPLACE, "");
	}
	
	public long getDataVolume() {
		return 1;
	}
	
	public void setDataVolume(long dataVolume) throws InvalidValueException {
		
	}
	
	public DataType getDataType() {
		return null;
	}
	
	public void setDataType(DataType dataType) throws InvalidValueException {
		
	}
	
	public long getDataCount() {
		return 1;
	}
	
	public void setDataCount(long dataCount) throws InvalidValueException {
		
	}
	
	public String getDataServiceClass() {
		return null;
	}
	
	public void setDataServiceClass(String dataServiceClass) throws InvalidValueException {
	}
	
	public String getDataServiceName() {
		return null;
	}
	
	public void setDataServiceName(String dataServiceName) throws InvalidValueException {
		
	}
	
	public String getDataServiceId() {
		return null;
	}
	
	public void setDataServiceId(String dataServiceId) throws InvalidValueException {
	}
	
	public URI getProviderId() {
		return null;
	}
	
	public void setProviderId(URI provideId) throws InvalidValueException {
	}
	
}
