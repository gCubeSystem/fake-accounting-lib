/**
 * 
 */
package org.gcube.accounting.datamodel.basetypes;

import java.io.Serializable;
import java.util.Map;

import org.gcube.accounting.datamodel.BasicUsageRecord;
import org.gcube.documentstore.exception.InvalidValueException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class AbstractJobUsageRecord extends BasicUsageRecord {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2361524354045135160L;

	public AbstractJobUsageRecord() {
		
	}
	
	public AbstractJobUsageRecord(Map<String,? extends Serializable> properties) throws InvalidValueException {
		
	}
	
	private static final String ABSTRACT_TO_REPLACE = "Abstract";
	
	@Override
	public String getRecordType() {
		return AbstractJobUsageRecord.class.getSimpleName().replace(ABSTRACT_TO_REPLACE, "");
	}
	
	public String getJobName() {
		return (String) null;
	}
	
	public void setJobName(String jobName) throws InvalidValueException {
		
	}
	
	public String getHost() {
		return null;
	}
	
	public void setHost(String host) throws InvalidValueException {
		
	}
	
	public String getServiceClass() {
		return null;
	}
	
	public void setServiceClass(String serviceClass) throws InvalidValueException {
		
	}
	
	public String getServiceName() {
		return null;
	}
	
	public void setServiceName(String serviceName) throws InvalidValueException {
		
	}
	
	public Long getDuration() {
		return null;
	}
	
	public void setDuration(Long duration) throws InvalidValueException {
		
	}
	
	public String getCallerQualifier() {
		return null;
	}
	
	public void setCallerQualifier(String callerQualifier) throws InvalidValueException {
		
	}
	
}
