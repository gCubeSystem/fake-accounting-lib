/**
 * 
 */
package org.gcube.accounting.datamodel.basetypes;

import java.io.Serializable;
import java.net.URI;
import java.util.Map;

import org.gcube.accounting.datamodel.BasicUsageRecord;
import org.gcube.documentstore.exception.InvalidValueException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class AbstractStorageUsageRecord extends BasicUsageRecord {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4538851408789372335L;

	public enum OperationType {
		CREATE, READ, UPDATE, DELETE
	}
	
	public enum DataType {
		STORAGE, TREE, GEO, DATABASE, OTHER
	}
	
	public AbstractStorageUsageRecord() {
	}
	
	public AbstractStorageUsageRecord(Map<String,? extends Serializable> properties) throws InvalidValueException {
	}
	
	private static final String ABSTRACT_TO_REPLACE = "Abstract";
	
	@Override
	public String getRecordType() {
		return AbstractStorageUsageRecord.class.getSimpleName().replace(ABSTRACT_TO_REPLACE, "");
	}
	
	public String getResourceOwner() {
		return null;
	}
	
	public void setResourceOwner(String owner) throws InvalidValueException {
	}
	
	public String getResourceScope() {
		return null;
	}
	
	public void setResourceScope(String scope) throws InvalidValueException {
		
	}
	
	public URI getProviderURI() {
		return null;
	}
	
	public void setProviderURI(URI providerURI) throws InvalidValueException {
		
	}
	
	public URI getResourceURI() {
		return null;
	}
	
	public void setResourceURI(URI resourceURI) throws InvalidValueException {
		
	}
	
	public OperationType getOperationType() {
		return null;
	}
	
	public void setOperationType(OperationType operationType) throws InvalidValueException {
	}
	
	public DataType getDataType() {
		return null;
	}
	
	public void setDataType(DataType dataType) throws InvalidValueException {
	}
	
	public long getDataVolume() {
		return 1;
	}
	
	public void setDataVolume(long dataVolume) throws InvalidValueException {
		
	}
	
	public String getQualifier() {
		return null;
	}
	
	public void setQualifier(String qualifier) throws InvalidValueException {
	}
	
}
