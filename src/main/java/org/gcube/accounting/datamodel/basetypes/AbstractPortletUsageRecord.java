/**
 * 
 */
package org.gcube.accounting.datamodel.basetypes;

import java.io.Serializable;
import java.util.Map;

import org.gcube.accounting.datamodel.BasicUsageRecord;
import org.gcube.documentstore.exception.InvalidValueException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class AbstractPortletUsageRecord extends BasicUsageRecord {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6521755521955331201L;

	public AbstractPortletUsageRecord() {
		
	}
	
	public AbstractPortletUsageRecord(Map<String,? extends Serializable> properties) throws InvalidValueException {
		
	}
	
	private static final String ABSTRACT_TO_REPLACE = "Abstract";
	
	@Override
	public String getRecordType() {
		return AbstractPortletUsageRecord.class.getSimpleName().replace(ABSTRACT_TO_REPLACE, "");
	}
	
	public String getPortletId() {
		return null;
	}
	
	public void setPortletId(String portletId) throws InvalidValueException {
		
	}
	
	public String getOperationId() {
		return null;
	}
	
	public void setOperationId(String operationId) throws InvalidValueException {
	}
	
	public String getMessage() {
		return null;
	}
	
	public void setMessage(String message) throws InvalidValueException {
	}
	
}
