package org.gcube.accounting.datamodel.usagerecords;

import java.io.Serializable;
import java.util.Map;

import org.gcube.accounting.datamodel.basetypes.AbstractJobUsageRecord;
import org.gcube.documentstore.exception.InvalidValueException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class JobUsageRecord extends AbstractJobUsageRecord {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2409704377456095140L;

	public JobUsageRecord() {
		super();
	}
	
	public JobUsageRecord(Map<String,? extends Serializable> properties) throws InvalidValueException {
		super(properties);
	}
}
