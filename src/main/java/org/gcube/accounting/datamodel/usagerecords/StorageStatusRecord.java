package org.gcube.accounting.datamodel.usagerecords;

import java.io.Serializable;
import java.util.Map;

import org.gcube.accounting.datamodel.basetypes.AbstractStorageStatusRecord;
import org.gcube.documentstore.exception.InvalidValueException;

/**
 * @author Alessandro Pieve (ISTI - CNR) alessandro.pieve@isti.cnr.it
 *
 */
public class StorageStatusRecord extends AbstractStorageStatusRecord {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7281637553846301944L;

	public StorageStatusRecord() {
		super();
	}
	
	public StorageStatusRecord(Map<String,? extends Serializable> properties) throws InvalidValueException {
		super(properties);
	}
}
