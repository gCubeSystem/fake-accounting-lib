package org.gcube.accounting.datamodel.usagerecords;

import java.io.Serializable;
import java.util.Map;

import org.gcube.accounting.datamodel.basetypes.AbstractPortletUsageRecord;
import org.gcube.documentstore.exception.InvalidValueException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class PortletUsageRecord extends AbstractPortletUsageRecord {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 236711996072341709L;

	public PortletUsageRecord() {
		super();
	}
	
	public PortletUsageRecord(Map<String,? extends Serializable> properties) throws InvalidValueException {
		super(properties);
	}
	
}
