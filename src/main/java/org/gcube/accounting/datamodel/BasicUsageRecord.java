/**
 * 
 */
package org.gcube.accounting.datamodel;

import java.io.Serializable;
import java.util.Map;

import org.gcube.documentstore.exception.InvalidValueException;
import org.gcube.documentstore.records.implementation.AbstractRecord;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class BasicUsageRecord extends AbstractRecord implements UsageRecord {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8414241700150395605L;

	public BasicUsageRecord() {
	}

	public BasicUsageRecord(Map<String, ? extends Serializable> properties) throws InvalidValueException {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getConsumerId() {
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setConsumerId(String consumerId) throws InvalidValueException {
		
	}

	@Override
	public String getRecordType() {
		return BasicUsageRecord.class.getSimpleName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getScope() {
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setScope(String scope) throws InvalidValueException {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OperationResult getOperationResult() {
		return OperationResult.SUCCESS;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws InvalidValueException
	 */

	@Override
	public void setOperationResult(OperationResult operationResult) throws InvalidValueException {
	}

}
