/**
 * 
 */
package org.gcube.accounting.persistence;

import java.util.concurrent.TimeUnit;

import org.gcube.documentstore.exception.InvalidValueException;
import org.gcube.documentstore.records.Record;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AccountingPersistence {
	
	protected AccountingPersistence(String context){
		
	}
	
	protected AccountingPersistence(){
		
	}

	public void account(final Record record) throws InvalidValueException {

	}
	
	public void flush() throws Exception {
		
	}
	
	/**
	 * Used {@link AccountingPersistence#flush() instead}
	 * @param timeout
	 * @param timeUnit
	 * @throws Exception
	 */
	@Deprecated
	public void flush(long timeout, TimeUnit timeUnit) throws Exception {
		this.flush();
	}
	
	public void close() throws Exception{

	}
	
	public boolean isConnectionActive() throws Exception {
		return true;
	}
		
}
