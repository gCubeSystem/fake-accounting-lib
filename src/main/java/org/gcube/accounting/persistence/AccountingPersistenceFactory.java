/**
 * 
 */
package org.gcube.accounting.persistence;

import java.util.concurrent.TimeUnit;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AccountingPersistenceFactory {

	private AccountingPersistenceFactory(){}
	
	
	
	public static void setFallbackLocation(String path){

	}
	
	public synchronized static AccountingPersistence getPersistence() {
		AccountingPersistence accountingPersistence = new AccountingPersistence();
		return accountingPersistence;
	}
	
	public static void flushAll(){
		
	}
	
	/**
	 * Use {@link AccountingPersistenceFactory#flushAll() instead}
	 * @param timeout
	 * @param timeUnit
	 */
	@Deprecated
	public static void flushAll(long timeout, TimeUnit timeUnit){
		AccountingPersistenceFactory.flushAll();
	}
	
	public static void shutDown(){
		//flush all and shutdown connection and thread
		
	}
	
	public static void shutDown(long timeout, TimeUnit timeUnit){
		
	}
}
